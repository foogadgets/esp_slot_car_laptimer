# About the ESP slot car timer

This is a Lap timer for Scalextric SSD slot cars. The result is presented as a webpage served by a web server running on a ESP8266 based module. A module called WeMos D1 mini have been tested.

The race is started in one of the following ways,

* Press letter **R** or

* Press the **Spacebar** or

* Click on the webpage where it says **PRESS HERE TO RACE!** or

* Click the physical button connected to D2 on the WeMos module


The All time high result is stored in the browser as a cookie, and is persistent until it is removed by pressing the button "Clear All time high".
The All time high can also be cleared by pressing keyboard key **C**

A false start is detected and halts the race if one contender is passing the start line within 0.1s after the start signal.

The Winner will be indicated by a flashing text. Also in the case of improving the All time high.

The physical sensors (one for each track) need to be connected to D3 and D4 on the WeMos.
More information (BOM, electrical schema) can be found [here](https://drive.google.com/drive/folders/0B3sf6ery1XoYbFZpZ091eU1xUXM?usp=sharing)
The sensors should be placed At or Right after the start/finishline. Cars should thus pass the sensors right after the start signal so that the lap timer detects Lap 0.



## Build and install
###Install ESP8266 support in Arduino IDE
Follow the instructions in this link, 

[https://github.com/esp8266/Arduino](https://github.com/esp8266/Arduino#installing-with-boards-manager)
###Install Websocket support
Unzip the following file into the folder *Arduino/library/*

[arduinoWebSockets-2.0.6.zip](https://github.com/Links2004/arduinoWebSockets/archive/2.0.6.zip)
###Install and enable asynchronous TCP support
Unzip the following file into the folder *Arduino/library/*

[ESPAsyncTCP zip-file](https://github.com/me-no-dev/ESPAsyncTCP/archive/master.zip)

Open the file Arduino/libraries/arduinoWebSockets/src/WebSockets.h in an editor.
Find the line that contains "#define WEBSOCKETS_NETWORK_TYPE NETWORK_ESP8266_ASYNC" and remove
the preceding forward slashes.
Make sure that after saving the file, the prefix (.h) is preserved.
###Install serial driver for the WeMos
Install the serial driver for your operating system,

[Serial driver](https://www.wemos.cc/downloads)

##Install the lap timer
Now the preparations are all complete and the only thing left is to configure and install the ESP slot car lap timer code onto the ESP8266 module.

Download the latest code and unzip it into any directory.

[latest zip](https://bitbucket.org/foogadgets/esp_slot_car_laptimer/get/master.zip)


Open the file esp_slot_car_laptimer.ino from the Arduino IDE.
Then select "tools" and then "CPU Frequency:". Choose 160MHz
Click the "Upload" button in the Arduino IDE, and the code will hopefully compile and upload to your WeMos module.

Once that is completed you connect the WeMos and sensor board, to any 5V micro USB charger you like. Within a couple of seconds, a webpage will be served.
The sensor board connects to "3V3", "G", "D3" and "D4" according to the instructions above.

The WeMos will create a wireless network with SSID "foogadgets" and passworkd "thereisnospoon". After connecting to the network, the Lap timer can be started by browsing to the address, [http://192.168.4.1/](http://192.168.4.1/)
If you have a macOS or Bonjour installed the lap timer can be accessed through the address, [http://racetimer.local](http://racetimer.local)

It is optional to connect a button between "G" and "D2" that provide a physical button to restart a race.


Have fun!

/Niclas
**foogadgets**