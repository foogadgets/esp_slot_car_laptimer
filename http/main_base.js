/*exported atFirstLoad, checkKey, removeATH, updateNoPlay, updateLaps, updateMinLaptime */
var wsUri = 'ws://192.168.4.1:81/';
var minLaptime = 1;
var noOfPlayers = 6;
var noOfLaps = 50;
var dv;
var car;
var lapTime;
var carIndex;
var wsConnection = 0;
var placementIndex = 0;
var topTime = '';
var racePosition = [];
var bestLaptimes = [999, 999, 999, 999, 999, 999];
var localLaps = [0, 0, 0, 0, 0, 0];
var totalRacetime = [0, 0, 0, 0, 0, 0];
var websocket;
var tmOut = [0,0,0,0];
var audioCtx = new (window.AudioContext || window.webkitAudioContext || window.audioContext)();

document.getElementById('noOfPlayers').value = noOfPlayers;
document.getElementById('noOfLaps').value = noOfLaps;
document.getElementById('minLaptime').value = minLaptime;
window.addEventListener('load', this.atFirstLoad, false);
window.addEventListener('keydown',this.checkKey,false);

function sumUpResults() {
    var message = 'All time high ';

    bestLaptimes.sort( function(a, b) {return (a-b);} );
    topTime = Number(localStorage.histBestTime);

    if ( '' === topTime || undefined === topTime ) {
        localStorage.histBestTime = 999;
        topTime = 999;
    }

    document.getElementById('toptime').innerHTML = 'Best race ' + bestLaptimes[0].toFixed(3) + 's';

    message += topTime.toFixed(3);
    if ( topTime > bestLaptimes[0] ) {
        localStorage.histBestTime = bestLaptimes[0];
        document.getElementById('athButton').innerHTML = 'Clear All time high';
        topTime = bestLaptimes[0];
        message = '<div class=\"area\">All time high ' + topTime.toFixed(3) + 's</div>';
    }
    document.getElementById('alltimehigh').innerHTML = message;
}

function restoreTable() {
    placementIndex = 0;
    topTime = Number(localStorage.histBestTime);
    var message = 'All time high ';
    if (topTime === 999 ) {
        document.getElementById('athButton').innerHTML = 'All time high Cleared';
        message += '-.-';
    } else {
        document.getElementById('athButton').innerHTML = 'Clear All time high';
        message += topTime.toFixed(3) + 's';
    }
    document.getElementById('alltimehigh').innerHTML = message;
    message = 'Best race -.-';
    document.getElementById('toptime').innerHTML = message;

    document.getElementById('countelement1').innerHTML = '';
    document.getElementById('countelement2').innerHTML = '';
    document.getElementById('countelement3').innerHTML = '';

    for(var i=1; i<=6; i++) {
        document.getElementById('Tname'+i).innerHTML = '';
        document.getElementById('Tlaps'+i).innerHTML = '';
        document.getElementById('Tlaptime'+i).innerHTML = '';
        document.getElementById('Tbestlap'+i).innerHTML = '';

        document.getElementById('name' + i).innerHTML = '';
        document.getElementById('laps'+i).innerHTML = '';
        document.getElementById('laptime'+i).innerHTML = '';
        document.getElementById('bestlap'+i).innerHTML = '';
    }
}

function ntohl(x) {
    return  ((x)<<24 & 0xFF000000) |
            ((x)<< 8 & 0x00FF0000) |
            ((x)>> 8 & 0x0000FF00) |
            ((x)>>24 & 0x000000FF);
}

function init() {
    if(wsConnection === 0) {
        wsConnection = 1;
        websocket = new WebSocket(wsUri);
        websocket.binaryType = 'arraybuffer';
    }

    websocket.send('raceStarted');

    websocket.onopen = function() {
        console.log('CONNECTED');
        websocket.send('raceStarted');
    };
    websocket.onclose = function() {
        console.log('DISCONNECTED');
        wsConnection = 0;
    };
    websocket.onerror = function(evt) {
        console.log('ERROR: ' + evt.data);
        wsConnection = 0;
    };
    websocket.onmessage = function(evt) {
        dv = new DataView(evt.data, 0, 8);
        car = dv.getUint8(0);
        lapTime = ntohl(dv.getUint32(4))/1000;
        carIndex = car-1;
        
        if( carIndex<noOfPlayers ) {

            if (lapTime == 0.1) {
                placementIndex++;
                document.getElementById('laps'+car).innerHTML = '<div class=\"area\" align=\"center\">False start!</div>';
                resetStartlight();
                websocket.send('raceEnded');
                document.getElementById('countelement2').innerHTML = '<center>PRESS HERE TO RACE!!!</center>';
                lapTime = 999;
                noOfPlayers = 0;
                localLaps[carIndex] = 0;
                return;
            }

            if ( (lapTime > minLaptime) | (!localLaps[carIndex]) ){

                if ( (lapTime < bestLaptimes[carIndex]) && (localLaps[carIndex]) ) {
                    bestLaptimes[carIndex] = lapTime;
                }

                if ( localLaps[carIndex] < noOfLaps ) {
                    var position = racePosition[localLaps[carIndex]]++;
                    document.getElementById('laps'   +car).innerHTML = localLaps[carIndex];
                    document.getElementById('name'   +car).innerHTML = position;
                    document.getElementById('pos'+position).innerHTML = '<div class=\"car' + carIndex + '\"></div>';
                    document.getElementById('laptime'+car).innerHTML = lapTime.toFixed(3) + 's';
                    document.getElementById('bestlap'+car).innerHTML = bestLaptimes[carIndex].toFixed(3) + 's';
                    if ( !localLaps[carIndex] ) document.getElementById('bestlap'+car).innerHTML = '-.-';

                    totalRacetime[carIndex] += lapTime;

                } else if ( localLaps[carIndex] == noOfLaps ) {
                    placementIndex++;
                    if (placementIndex == 1) document.getElementById('laps'+car).innerHTML = '<div class=\"area\" align=\"center\">Finished ' + placementIndex + '</div>';
                    else document.getElementById('laps'+car).innerHTML = '<div align=\"center\">Finished ' + placementIndex + '</div>';
                    document.getElementById('laptime'+car).innerHTML = lapTime.toFixed(3) + 's';
                    document.getElementById('bestlap'+car).innerHTML = bestLaptimes[carIndex].toFixed(3) + 's';

                    totalRacetime[carIndex] += lapTime;
                }

                localLaps[carIndex]++;
            }
            
            if (noOfPlayers == placementIndex) {
                websocket.send('raceEnded');
                document.getElementById('countelement2').innerHTML = '<center>PRESS HERE TO RACE!!!</center>';
                sumUpResults();
            }
        }
        if (car === 99) {
            restartRace();
        }
    };
}

function startRace() {
    restoreTable();
    bestLaptimes = [999, 999, 999, 999, 999, 999];
    noOfPlayers = document.getElementById('noOfPlayers').value;
    noOfLaps = document.getElementById('noOfLaps').value;
    minLaptime = document.getElementById('minLaptime').value;
    localLaps = [0, 0, 0, 0, 0, 0];
    for (var j=0; j<noOfLaps; j++) {
        racePosition[j] = 1;
    }

    init();

    for (var i=1; i<=noOfPlayers; i++) {
        document.getElementById('Tname' + i).innerHTML = 'Driver ' + i + '  Pos ';
        document.getElementById('Tlaps' + i).innerHTML = 'Laps';
        document.getElementById('Tlaptime' + i).innerHTML = 'Last';
        document.getElementById('Tbestlap' + i).innerHTML = 'Best';
        
        document.getElementById('laps' + i).innerHTML = '-';
        document.getElementById('name' + i).innerHTML = '-';
        document.getElementById('laptime' + i).innerHTML = '-.-';
        document.getElementById('bestlap' + i).innerHTML = '-.-';
    }

    var message = '<div class=\"countelement';
    var randTime = Math.random();

    tmOut[0] = setTimeout(function() {
        document.getElementById('countelement1').innerHTML = message + 'red\"></div>';
        beep(1700);
        }, 1*1000 );
    tmOut[1] = setTimeout(function() {
        document.getElementById('countelement2').innerHTML = message + 'red\"></div>';
        beep(1700);
        }, 2*1000 );
    tmOut[2] = setTimeout(function() {
        document.getElementById('countelement3').innerHTML = message + 'red\"></div>';
        beep(1700);
        }, 3*1000 );
    tmOut[3] = setTimeout(function() {
        document.getElementById('countelement1').innerHTML = message + 'green\"></div>';
        document.getElementById('countelement2').innerHTML = message + 'green\"></div>';
        document.getElementById('countelement3').innerHTML = message + 'green\"></div>';
        websocket.send('okToGo');
        beep(3400);
        }, (5+randTime)*1000 );
}

function atFirstLoad() {
    websocket = new WebSocket(wsUri);
    websocket.binaryType = 'arraybuffer';
    wsConnection = 1;
}

function beep(freq) {
    var oscillator = audioCtx.createOscillator();
    var gainNode = audioCtx.createGain();

    oscillator.connect(gainNode);
    gainNode.connect(audioCtx.destination);

    gainNode.gain.value = 0.5;
    oscillator.frequency.value = freq;
    oscillator.type = 'sine';

    oscillator.start();

    setTimeout( function() {oscillator.stop();}, 180);
}

function resetStartlight() {
    for (var i=0; i<4; i++) {
        clearTimeout(tmOut[i]);
    }
}

function restartRace() {
    resetStartlight();
    startRace();
}

function checkKey(e) {
    var code = (typeof e.which == 'number') ? e.which : e.keyCode;
    switch (code) {
        case 32:
        case 82:
            restartRace();
            break;
        case 67:
            removeATH();
            break;
        default:
            break;
    }
}

function removeATH() {
    localStorage.histBestTime = 999;
    document.getElementById('athButton').innerHTML = 'All time high Cleared';
    document.getElementById('alltimehigh').innerHTML = 'All time high -.-';
}

function updateNoPlay() {
	noOfPlayers = document.getElementById('noOfPlayers').value;
	document.getElementById('noOfPlayers').defaultValue = noOfPlayers;
}

function updateLaps() {
	noOfLaps = document.getElementById('noOfLaps').value;
	document.getElementById('noOfLaps').defaultValue = noOfLaps;
}

function updateMinLaptime() {
	minLaptime = document.getElementById('minLaptime').value;
	document.getElementById('minLaptime').defaultValue = minLaptime;    
}