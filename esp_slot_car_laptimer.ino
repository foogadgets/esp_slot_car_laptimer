/*
   esp_slotcar_timer.ino
   http://racetimer.local/

   Created: 9.1.2017

  Build information: http/gen_compact_index_file.sh need to be executed after
  any change to index_base.html or main_base.js

*/

#include <Arduino.h>

#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <WebSocketsServer.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <Hash.h>
#include "webpage.h"
#include "wifi_credentials.h"

#define EXT_SENS  // Define if another sensor board than foogadgets sensor board will be used.




extern "C" {  // To enable os_timer_t
#include "os_type.h"
}

#define TRIG_MODE   FALLING // RISING or FALLING
#define LANE_RELAX   10     // ms to relax after a car detection
#define WASTED_READS  3     // Let car drive closer to LED for a better reading
#define FALSESTART  100     // False start time in milliseconds
#define STARTPAUSE  (600-FALSESTART) // 0.6s should be enough to get over startline
#define PULSELENGTH 46
#define HALFPULSLEN 23    // PULSELENGTH/2
#define MAXSIGNAL   (9*PULSELENGTH)
#define MINSIGNAL   (3*PULSELENGTH)

#ifdef EXT_SENS
#define IR_LANE_1   0     // GPIO0 (D3 on WeMos)
#define IR_LANE_2   2     // GPIO2 (D4 on WeMos)
#define RESTART     4     // GPIO4 (D2 on WeMos)
#else
#define IR_LANE_1   4     // GPIO5 (D2 on WeMos)
#define IR_LANE_2   5     // GPIO4 (D1 on WeMos)
#define RESTART     0     // GPIO0 (D3 on WeMos)
#endif

#define ANALOG_MOD  16    // GPIO16 (D0 on WeMos)
#define DRIVERS     6     // Max amount of race drivers
#define USE_SERIAL Serial

ESP8266WiFiMulti WiFiMulti;
ESP8266WebServer server = ESP8266WebServer(80);
WebSocketsServer webSocket = WebSocketsServer(81);

unsigned long prevLapTime[DRIVERS + 1];
unsigned long raceStartTime;
os_timer_t myTimer[2];
volatile uint8_t socketNum = 0;
volatile uint8_t carID = 0;
volatile uint8_t laneEvent = 0;
uint8_t laneIndex = 0;
uint8_t raceOn = 0;
uint8_t restartFlag = 0;
volatile uint8_t wasteRead[2] = {0, 0};
volatile unsigned long detectMicroTime[2] = {0, 0};
volatile unsigned long prevIntr[2] = {0, 0};
volatile long deltaTu[2] = {0, 0};
long deltaTm = 0;
int connections = 0;
volatile unsigned long debounce_millis = 0;
long sendBuf[2] = {0, 0};
uint8_t* p_data;


// ===== Function declarations ===== //
void cleanEndedRace(void);
void lane1Passing(void)ICACHE_FLASH_ATTR;
void lane2Passing(void)ICACHE_FLASH_ATTR;
void handleFinishLine(void);
void restartRace(void)ICACHE_FLASH_ATTR;
void checkCarID(uint8_t);
// ~^~^~ Function declarations ~^~^~ //


void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t lenght) {
  switch (type) {
    case WStype_DISCONNECTED:
      USE_SERIAL.printf("[%u] Disconnected!\n", num);
      connections--;
      break;
    case WStype_CONNECTED: {
        IPAddress ip = webSocket.remoteIP(num);
        USE_SERIAL.printf("[%u] Connected from %d.%d.%d.%d url: %s\n", num, ip[0], ip[1], ip[2], ip[3], payload);

        // send message to client
        webSocket.sendBIN(num, &num, 1);
        socketNum = num;
        connections++;
      }
      break;
    case WStype_TEXT:
      String text = String((char *) &payload[0]);
      USE_SERIAL.printf("[%u] get Text: %s\n", num, payload);
      if (num == 0) {
        if (text == "raceStarted") {
          attachInterrupt(digitalPinToInterrupt(IR_LANE_1), lane1Passing, TRIG_MODE);
          attachInterrupt(digitalPinToInterrupt(IR_LANE_2), lane2Passing, TRIG_MODE);
        }
        if (text == "okToGo") {
          raceStartTime = micros();
          for (uint8_t i = 0; i <= DRIVERS; i++) prevLapTime[i] = raceStartTime;
          raceOn = 1;
        }
        if (text == "raceEnded") {
          cleanEndedRace();
        }
        if (text == "settings") {
          for (uint8_t i = 1; i < connections; i++) {
            webSocket.sendTXT(i, payload);
          }
        }
      }
      break;
  }
}

// ===== Interrupt routines ===== //
void ICACHE_FLASH_ATTR
lane1Passing(void) {
  detectMicroTime[0] = micros();
  if (WASTED_READS < wasteRead[0]++) {
    deltaTu[0] = detectMicroTime[0] - prevIntr[0];
    bitSet(laneEvent, 0);
  }
  prevIntr[0] = detectMicroTime[0];
}

void ICACHE_FLASH_ATTR
lane2Passing(void) {
  detectMicroTime[1] = micros();
  if (WASTED_READS < wasteRead[1]++) {
    deltaTu[1] = detectMicroTime[1] - prevIntr[1];
    bitSet(laneEvent, 1);
  }
  prevIntr[1] = detectMicroTime[1];
}
// ~^~^~ Interrupt routines ~^~^~ //


void timerCallback1(void *pArg) {
  attachInterrupt(digitalPinToInterrupt(IR_LANE_1), lane1Passing, TRIG_MODE);
  bitClear(laneEvent, 0);
} // End of timerCallback1

void timerCallback2(void *pArg) {
  attachInterrupt(digitalPinToInterrupt(IR_LANE_2), lane2Passing, TRIG_MODE);
  bitClear(laneEvent, 1);
} // End of timerCallback2


//// Function return carID (1-6) or 0 if not valid
void checkCarID(uint8_t lane) {
  if (deltaTu[lane] < 0) deltaTu[lane] += 0xffffffffL;
  deltaTu[lane] -= HALFPULSLEN;
  if ((deltaTu[lane] < MAXSIGNAL) && (deltaTu[lane] > MINSIGNAL)) {  // 3xPulseLen > deltaTu > 9xPulseLen
#ifdef EXT_SENS
    detachInterrupt(digitalPinToInterrupt(2 * lane));
#else
    detachInterrupt(digitalPinToInterrupt(4 + lane));
#endif
    os_timer_arm(&myTimer[lane], LANE_RELAX, NULL); // Relax for LANE_RELAX ms and let car pass
    carID = (((uint8_t)(deltaTu[lane] / PULSELENGTH)) - 2);
    bitClear(laneEvent, lane);
    wasteRead[lane] = 0;
  }
}


void handleFinishLine() {
  while (laneEvent & 3) {
    laneIndex = (laneEvent >> 1);  // 0x01 or 0x00
    checkCarID(laneIndex);

    if (carID) {
      deltaTm = detectMicroTime[laneIndex] - prevLapTime[carID];
      prevLapTime[carID] = detectMicroTime[laneIndex];

      sendBuf[0] = carID;
      carID = 0;

      if (deltaTm < 0) deltaTm += 0xffffffffL;
      deltaTm >>= 10; // Divide by 1024 is good enough :)

      if (raceOn) {
        sendBuf[1] = deltaTm;
      } else {
        sendBuf[1] = FALSESTART;
      }
      webSocket.broadcastBIN(p_data, 8);
    }
  }

  if (restartFlag) {
    restartFlag = 0;
    sendBuf[0] = 99L;
    sendBuf[1] = 0L;
    webSocket.broadcastBIN(p_data, 8);
    cleanEndedRace();
  }
}


void cleanEndedRace() {
  os_timer_disarm(&myTimer[0]);
  os_timer_disarm(&myTimer[1]);
  detachInterrupt(digitalPinToInterrupt(IR_LANE_1));
  detachInterrupt(digitalPinToInterrupt(IR_LANE_2));
  raceOn = 0;
  laneEvent = 0;
  wasteRead[0] = 0;
  wasteRead[1] = 0;
}

void ICACHE_FLASH_ATTR
restartRace() {  // Interrupt routine to restart race
  if ((long)(millis() - debounce_millis) > 100) { // debounce
    restartFlag = 1;
    debounce_millis = millis();
  }
}

//===================
void setup() {
  USE_SERIAL.begin(115200);
  USE_SERIAL.println();
  USE_SERIAL.println();
  USE_SERIAL.println();

  for (uint8_t t = 3; t > 0; t--) {
    USE_SERIAL.printf("[SETUP] BOOT WAIT %d...\n", t);
    USE_SERIAL.flush();
    delay(1000);
  }

  pinMode(IR_LANE_1, INPUT);
  os_timer_disarm(&myTimer[0]);
  os_timer_setfn(&myTimer[0], timerCallback1, NULL);
  pinMode(IR_LANE_2, INPUT);
  os_timer_disarm(&myTimer[1]);
  os_timer_setfn(&myTimer[1], timerCallback2, NULL);

  pinMode(RESTART, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(RESTART), restartRace, FALLING);

  pinMode(ANALOG_MOD, OUTPUT);
  digitalWrite(ANALOG_MOD, LOW);

  WiFi.softAP(ssid, password);

  // start webSocket server
  webSocket.begin();
  webSocket.onEvent(webSocketEvent);

  USE_SERIAL.print("IP Address: ");
  USE_SERIAL.println(WiFi.softAPIP());

  if (MDNS.begin("racetimer")) { // http://racetimer.local/
    USE_SERIAL.println("MDNS responder started");
  }

  // handle index
  server.on("/", []() {
    server.send_P(200, "text/html", index_html);
  });

  server.begin();

  // Add service to MDNS
  MDNS.addService("http", "tcp", 80);
  MDNS.addService("ws", "tcp", 81);

  raceStartTime = micros();

  for (uint8_t i = 0; i <= DRIVERS; i++) {
    prevLapTime[i] = raceStartTime;
  }

  p_data = (uint8_t*)&sendBuf[0];
}

void loop() {
  server.handleClient();
  handleFinishLine();
  yield();
}

